Parse.initialize("XtSczhe7A7jdfnsLcJpXRQ72xaDvs6nrYuDWwLf4", "q2K1lSmIWSKGYcCXIlsZ6EGnPX590xRIO2fjfusY", "gt9JBEdsDiFXcOuC7FQIzKRCRQlIrJguJWHvsoKZ"); //PASTE HERE YOUR Back4App APPLICATION ID AND YOUR JavaScript KEY
Parse.serverURL = "https://parseapi.back4app.com/";

/*
    *** Only used in the initial construction of the leaderboard
*/

/*
Initializing each row in the leaderboard class
Associating each row with a dog pointer
*/

var dogQuery = new Parse.Query("Dog");
dogQuery.each(function (dog){
    var Leaderboard = Parse.Object.extend("Leaderboard");
    var leaderboard = new Leaderboard();
    leaderboard.set("Dog", dog);
    return leaderboard.save();
}).then(function (){
    console.log("Success");
}, function (err){
    console.error(err);
});


/*
Creating the lifetime statistics columns
    Total Minutes
    Total Dispenses
*/

var lifetimeQuery = new Parse.Query("Leaderboard");
lifetimeQuery.include("Dog");
lifetimeQuery.each(function (dog){
    if (dog.get("Dog").get("lifetimePlayTimeInMillis") == undefined){
        dog.set("lifetimePlayTimeInMillis", 0);
    } else{
        dog.set("lifetimePlayTimeInMillis", dog.get("Dog").get("lifetimePlayTimeInMillis"));  
    }
    if (dog.get("Dog").get("lifetimeDispenseCount") == undefined){
        dog.set("lifetimeDispenseCount", 0);
    } else{
        dog.set("lifetimeDispenseCount", dog.get("Dog").get("lifetimeDispenseCount"));
    }

    return dog.save();
}).then(function (){
    console.log("Success");
}, function (err){
    console.error(err);
});

/*
    Creating initial Level 1 statistics columns
        Level 1 Minutes
        Level 1 Dispenses
*/

var leaderboardQuery = new Parse.Query("Leaderboard");
var sessionQuery = new Parse.Query("GamePlaySession");

leaderboardQuery.include("Dog");
sessionQuery.include(["dispenseCount", "createdAt", "sessionEndTime"]);
leaderboardQuery.limit(1000);

var promises = [];
var dogs = [];

leaderboardQuery.find().then(function (results){
    dogs = results;
    level1Entries(dogs);
}, function error(err){
    console.error(err);
});

function level1Entries(objects){
    var targetObject = objects.shift();
    if(targetObject){
        var dogId = targetObject.get("Dog").id;

        sessionQuery.equalTo("dogObjectId", dogId);
        sessionQuery.equalTo("currentLevel", 1);
        sessionQuery.limit(1000);
        sessionQuery.select("dispenseCount");
        sessionQuery.select("createdAt");
        sessionQuery.select("sessionEndTime");

        var dispenses = 0;
        var gameTime = 0;

        sessionQuery.find().then(function (sessions){
            for(var i = 0; i < sessions.length; i++){
                dispense = sessions[i].get("dispenseCount");
                var startSeconds = new Date(sessions[i].get("createdAt")).getTime();
                var endSeconds = new Date(sessions[i].get("sessionEndTime")).getTime();
                if(dispense != undefined){
                    dispenses += dispense;
                }
                if(!isNaN(startSeconds) && !isNaN(endSeconds)){
                    gameTime += (endSeconds - startSeconds)/1000;
                }

            }
        }).then(function (){
            targetObject.set("level1Dispenses", dispenses);
            targetObject.set("level1Seconds", gameTime);
            targetObject.save();
        }).then(function (){
            level1Entries(objects);
        });  
    }
};

/*
    Creating Level 1 score column
*/

var lifetimeQuery = new Parse.Query("Leaderboard");
var DISPENSE_THRESHOLD = 10;

lifetimeQuery.include("Dog");
lifetimeQuery.each(function (dog){
    var dispenses = dog.get("level1Dispenses");
    var seconds = dog.get("level1Seconds");
    
    if(seconds < 0 || dispenses < DISPENSE_THRESHOLD){
        dog.set("level1Score", undefined);
    }else{
        dog.set("level1Score", seconds/dispenses);
    }

    return dog.save();
}).then(function (){
    console.log("Success");
}, function (err){
    console.error(err);
});

/*
    Creating initial Level 2 statistics columns
        Level 2 Minutes
        Level 2 Dispenses
*/

var leaderboardQuery = new Parse.Query("Leaderboard");
var sessionQuery = new Parse.Query("GamePlaySession");

leaderboardQuery.include("Dog");
sessionQuery.include(["dispenseCount", "createdAt", "sessionEndTime"]);
leaderboardQuery.limit(1000);

var promises = [];
var dogs = [];

leaderboardQuery.find().then(function (results){
    dogs = results;
    level2Entries(dogs);
}, function error(err){
    console.error(err);
});

function level2Entries(objects){
    var targetObject = objects.shift();
    if(targetObject){
        var dogId = targetObject.get("Dog").id;

        sessionQuery.equalTo("dogObjectId", dogId);
        sessionQuery.equalTo("currentLevel", 2);
        sessionQuery.limit(1000);
        sessionQuery.select("dispenseCount");
        sessionQuery.select("createdAt");
        sessionQuery.select("sessionEndTime");

        var dispenses = 0;
        var gameTime = 0;

        sessionQuery.find().then(function (sessions){
            for(var i = 0; i < sessions.length; i++){
                dispense = sessions[i].get("dispenseCount");
                var startSeconds = new Date(sessions[i].get("createdAt")).getTime();
                var endSeconds = new Date(sessions[i].get("sessionEndTime")).getTime();
                if(dispense != undefined){
                    dispenses += dispense;
                }
                if(!isNaN(startSeconds) && !isNaN(endSeconds)){
                    gameTime += (endSeconds - startSeconds)/1000;
                }

            }
        }).then(function (){
            targetObject.set("level2Dispenses", dispenses);
            targetObject.set("level2Seconds", gameTime);
            targetObject.save();
        }).then(function (){
            level2Entries(objects);
        });  
    }
};

/*
    Creating Level 2 score column
*/

var lifetimeQuery = new Parse.Query("Leaderboard");
var DISPENSE_THRESHOLD = 10;

lifetimeQuery.include("Dog");
lifetimeQuery.each(function (dog){
    var dispenses = dog.get("level2Dispenses");
    var seconds = dog.get("level2Seconds");
    
    if(seconds < 0 || dispenses < DISPENSE_THRESHOLD){
        dog.set("level2Score", undefined);
    }else{
        dog.set("level2Score", seconds/dispenses);
    }

    return dog.save();
}).then(function (){
    console.log("Success");
}, function (err){
    console.error(err);
});

/*
    Creating initial Level 3 statistics columns
        Level 3 Minutes
        Level 3 Dispenses
        Level 3 Score
*/

var leaderboardQuery = new Parse.Query("Leaderboard");
var sessionQuery = new Parse.Query("GamePlaySession");

leaderboardQuery.include("Dog");
sessionQuery.include(["dispenseCount", "createdAt", "sessionEndTime"]);
sessionQuery.include(["missesCount", "beepsCount"]);
leaderboardQuery.limit(1000);

var promises = [];
var dogs = [];

leaderboardQuery.find().then(function (results){
    dogs = results;
    level3Entries(dogs);
}, function error(err){
    console.error(err);
});

function level3Entries(objects){
    var targetObject = objects.shift();
    if(targetObject){
        var dogId = targetObject.get("Dog").id;

        sessionQuery.equalTo("dogObjectId", dogId);
        sessionQuery.equalTo("currentLevel", 3);
        sessionQuery.limit(1000);
        sessionQuery.select("dispenseCount");
        sessionQuery.select("createdAt");
        sessionQuery.select("sessionEndTime");
        sessionQuery.select("missesCount");
        sessionQuery.select("beepsCount");

        var dispenses = 0;
        var gameTime = 0;
        var missed = 0;
        var total = 0;

        sessionQuery.find().then(function (sessions){
            for(var i = 0; i < sessions.length; i++){
                var dispense = sessions[i].get("dispenseCount");
                var startSeconds = new Date(sessions[i].get("createdAt")).getTime();
                var endSeconds = new Date(sessions[i].get("sessionEndTime")).getTime();
                var sessionMissed = sessions[i].get("missesCount");
                var sessionTotal = sessions[i].get("beepsCount");

                if(dispense != undefined){
                    dispenses += dispense;
                }
                if(!isNaN(startSeconds) && !isNaN(endSeconds)){
                    gameTime += (endSeconds - startSeconds)/1000;
                }
                if(!isNaN(sessionMissed) && !isNaN(sessionTotal)){
                    missed += sessionMissed;
                    total += sessionTotal;
                }

            }
        }).then(function (){
            targetObject.set("level3Dispenses", dispenses);
            targetObject.set("level3Seconds", gameTime);
            targetObject.set("level3Score", ((total-missed)/total));
            targetObject.save();
        }).then(function (){
            level3Entries(objects);
        });  
    }
};

/*
    Creating initial Level 4 statistics columns
        Level 4 Minutes
        Level 4 Dispenses
        Level 4 Score
*/

var leaderboardQuery = new Parse.Query("Leaderboard");
var sessionQuery = new Parse.Query("GamePlaySession");

leaderboardQuery.include("Dog");
sessionQuery.include(["dispenseCount", "createdAt", "sessionEndTime"]);
sessionQuery.include(["missesCount", "beepsCount"]);
leaderboardQuery.limit(1000);

var promises = [];
var dogs = [];

leaderboardQuery.find().then(function (results){
    dogs = results;
    level4Entries(dogs);
}, function error(err){
    console.error(err);
});

function level4Entries(objects){
    var targetObject = objects.shift();
    if(targetObject){
        var dogId = targetObject.get("Dog").id;

        sessionQuery.equalTo("dogObjectId", dogId);
        sessionQuery.equalTo("currentLevel", 4);
        sessionQuery.limit(1000);
        sessionQuery.select("dispenseCount");
        sessionQuery.select("createdAt");
        sessionQuery.select("sessionEndTime");
        sessionQuery.select("missesCount");
        sessionQuery.select("beepsCount");

        var dispenses = 0;
        var gameTime = 0;
        var missed = 0;
        var total = 0;

        sessionQuery.find().then(function (sessions){
            for(var i = 0; i < sessions.length; i++){
                var dispense = sessions[i].get("dispenseCount");
                var startSeconds = new Date(sessions[i].get("createdAt")).getTime();
                var endSeconds = new Date(sessions[i].get("sessionEndTime")).getTime();
                var sessionMissed = sessions[i].get("missesCount");
                var sessionTotal = sessions[i].get("beepsCount");

                if(dispense != undefined){
                    dispenses += dispense;
                }
                if(!isNaN(startSeconds) && !isNaN(endSeconds)){
                    gameTime += (endSeconds - startSeconds)/1000;
                }
                if(!isNaN(sessionMissed) && !isNaN(sessionTotal)){
                    missed += sessionMissed;
                    total += sessionTotal;
                }

            }
        }).then(function (){
            targetObject.set("level4Dispenses", dispenses);
            targetObject.set("level4Seconds", gameTime);
            targetObject.set("level4Score", ((total-missed)/total));
            targetObject.set("level4Beeps", total);
            targetObject.save();
        }).then(function (){
            level4Entries(objects);
        });  
    }
};

/*
    Creating initial Level 5 statistics columns
        Level 5 Minutes
        Level 5 Dispenses
        Level 5 Score
*/

var leaderboardQuery = new Parse.Query("Leaderboard");
var sessionQuery = new Parse.Query("GamePlaySession");

leaderboardQuery.include("Dog");
sessionQuery.include(["dispenseCount", "createdAt", "sessionEndTime"]);
sessionQuery.include(["missesCount", "beepsCount"]);
leaderboardQuery.limit(1000);

var promises = [];
var dogs = [];

leaderboardQuery.find().then(function (results){
    dogs = results;
    level5Entries(dogs);
}, function error(err){
    console.error(err);
});

function level5Entries(objects){
    var targetObject = objects.shift();
    if(targetObject){
        var dogId = targetObject.get("Dog").id;

        sessionQuery.equalTo("dogObjectId", dogId);
        sessionQuery.equalTo("currentLevel", 5);
        sessionQuery.limit(1000);
        sessionQuery.select("dispenseCount");
        sessionQuery.select("createdAt");
        sessionQuery.select("sessionEndTime");
        sessionQuery.select("missesCount");
        sessionQuery.select("beepsCount");

        var dispenses = 0;
        var gameTime = 0;
        var missed = 0;
        var total = 0;

        sessionQuery.find().then(function (sessions){
            for(var i = 0; i < sessions.length; i++){
                var dispense = sessions[i].get("dispenseCount");
                var startSeconds = new Date(sessions[i].get("createdAt")).getTime();
                var endSeconds = new Date(sessions[i].get("sessionEndTime")).getTime();
                var sessionMissed = sessions[i].get("missesCount");
                var sessionTotal = sessions[i].get("beepsCount");

                if(dispense != undefined){
                    dispenses += dispense;
                }
                if(!isNaN(startSeconds) && !isNaN(endSeconds)){
                    gameTime += (endSeconds - startSeconds)/1000;
                }
                if(!isNaN(sessionMissed) && !isNaN(sessionTotal)){
                    missed += sessionMissed;
                    total += sessionTotal;
                }

            }
        }).then(function (){
            targetObject.set("level5Dispenses", dispenses);
            targetObject.set("level5Seconds", gameTime);
            targetObject.set("level5Score", ((total-missed)/total));
            targetObject.set("level5Beeps", total);
            targetObject.save();
        }).then(function (){
            level5Entries(objects);
        });  
    }
};

/*
    Insert Dog IDs
*/

Parse.Cloud.run("dogID").then(function(response){
    console.log(response);
});

/*
    Insert Shaping Score
*/

Parse.Cloud.run("shapingScore").then(function(response){
    console.log(response);
});

var leaderboardQuery = new Parse.Query("Leaderboard");
var shapingQuery = new Parse.Query("ShapingEvent");
var test = 1;

leaderboardQuery.each(function (dog){
    var dogId = dog.get("dogObjectId");
    test = Math.round(Math.random()*5);
    shapingQuery.equalTo("eventType", "Motion");
    shapingQuery.equalTo("hasBeepedAtleastOnce", "true");
    shapingQuery.containedIn("gameLevel", ["3","4","5"]);
    shapingQuery.equalTo("dogObjectId", dogId);
    shapingQuery.limit(1000);

    var level3Success = 0;
    var level3Failure = 0;
    var level4Success = 0;
    var level4Failure = 0;
    var level5Success = 0;
    var level5Failure = 0;

    var promise = shapingQuery.find();
    
    promise.then(function (events){
        console.log(events);
        for(var i = 0; i < events.length; i++){
            var level = parseInt(events[i].get("gameLevel"));
            var time = events[i].get("millisSinceLastBeep");
            var lastBeep = events[i].get("wasLastBeepPositive");
            if(level == 3){
                if(time < 3000){
                    level3Success++;
                }else{
                    level3Failure++;
                }
            }else if(level == 4){
                if(lastBeep == true){
                    level4Success++;
                }else{
                    level4Failure++;
                }
            }else if(level == 5){
                if(lastBeep == true){
                    level5Success++;
                }else{
                    level5Failure++;
                }
            }
        }
    }).then(function (){
        dog.set("level3ShapingSuccess", level3Success);
        dog.set("level3ShapingFailure", level3Failure);
        dog.set("level4ShapingSuccess", level4Success);
        dog.set("level4ShapingFailure", level4Failure);
        dog.set("level5ShapingSuccess", level5Success);
        dog.set("level5ShapingFailure", level5Failure);

        dog.set("level3ShapingScore", level3Success / (level3Success + level3Failure));
        dog.set("level4ShapingScore", level4Success / (level4Success + level4Failure));
        dog.set("level5ShapingScore", level5Success / (level5Success + level5Failure));

        return dog.save();
        promise.resolve(); //Need this to have it keep looping until everything is updated
    });

    return promise;
});

/*
    Testing
*/

// var Session = new Parse.Object.extend("GamePlaySession");
// var session = new Session();
// var sessionData = {
//     "sessionEndTime": new Date("2018-07-25T23:25:00"),
//     "successRate": 50,
//     "missesCount": 5,
//     "beepsCount": 10,
//     "currentLevel": 1,
//     "sessionFinalized": true,
//     "dispenseCount": 4,
//     "dogObjectId": "d6CvaTwQ6F"
// };

// session.save(sessionData,{
//     success: function(obj){
//         console.log(obj);
//     },
//     error: function(obj, err){
//         console.error(err);
//     }
// });

