//Parse.initialize("4eP36OQivECsD2ssb0040WwRDlMXj2VhcRuZr58N", "9Xahf6CL1Km3a5GaMa8b8C5srjg9mDuhBHUSHB6O", "yM3t0eEgCtO7zVh021QpmthG4VJ9YgWlbnZCD9Rm"); //PASTE HERE YOUR Back4App APPLICATION ID AND YOUR JavaScript KEY
Parse.initialize("XtSczhe7A7jdfnsLcJpXRQ72xaDvs6nrYuDWwLf4", "q2K1lSmIWSKGYcCXIlsZ6EGnPX590xRIO2fjfusY", "gt9JBEdsDiFXcOuC7FQIzKRCRQlIrJguJWHvsoKZ"); //PASTE HERE YOUR Back4App APPLICATION ID AND YOUR JavaScript KEY
Parse.serverURL = "https://parseapi.back4app.com/";

/*
    Wrote the functions in this file to compute various questions about the dog data
*/

// var threshold5 = 0;
// var threshold10 = 0;
// var threshold19 = 0;

// var dogs5 = [];
// var dogs10 = [];
// var dogs19 = [];

var sessionCount = 0;

var sessionQuery = new Parse.Query("GamePlaySession");

sessionQuery.include(["createdAt", "sessionEndTime", "dogObjectId"]);
sessionQuery.equalTo("sessionFinalized", true);
sessionQuery.greaterThan("createdAt", new Date(2017, 10, 1));

sessionQuery.each(function (session){
    //var dogId = session.get("dogObjectId");
    var startSeconds = new Date(session.get("createdAt")).getTime();
    var endSeconds = new Date(session.get("sessionEndTime")).getTime();
    var time = endSeconds - startSeconds;
    // if(time > 300000 && !dogs5.includes(dogId)){
    //     dogs5.push(dogId);
    // }
    // if(time > 600000 && !dogs10.includes(dogId)){
    //     dogs10.push(dogId);
    // }
    // if(time > 1140000 && !dogs19.includes(dogId)){
    //     dogs19.push(dogId);
    // }
    if(time > 900000 && time <= 1140000){
        sessionCount++;
    }
    Parse.Promise.resolve();

}).then(function () {
    // Set the job's success status
    // console.log(">5 mins: ", dogs5.length);
    // console.log(">10 mins: ", dogs10.length);
    // console.log(">19 mins: ", dogs19.length);
    // console.log(dogs19);
    console.log(sessionCount);
}, function (err) {
    // Set the job's error status
    console.error("Error");
});


/*
    Creating initial Level 5 statistics columns
        Level 5 Minutes
        Level 5 Dispenses
        Level 5 Score
*/

// var dogQuery = new Parse.Query("Dog");
// var sessionQuery = new Parse.Query("GamePlaySession");

// dogQuery.include(["numSessions", "minSessionTime", "maxSessionTime"]);
// sessionQuery.include(["createdAt", "sessionEndTime"]);
// dogQuery.limit(1000);

// var dogs = [];

// dogQuery.find().then(function (results){
//     dogs = results;
//     console.log(dogs);
//     statEntries(dogs);
// }, function error(err){
//     console.error(err);
// });

// function statEntries(objects){
//     var targetObject = objects.shift();
//     if(targetObject){
//         var dogId = targetObject.id;

//         sessionQuery.equalTo("dogObjectId", dogId);
//         sessionQuery.limit(1000);
//         sessionQuery.select("createdAt");
//         sessionQuery.select("sessionEndTime");

//         var minTime = Infinity;
//         var maxTime = 0;
//         var numSessions = 0;

//         sessionQuery.find().then(function (sessions){
//             for(var i = 0; i < sessions.length; i++){
//                 var startSeconds = new Date(sessions[i].get("createdAt")).getTime();
//                 var endSeconds = new Date(sessions[i].get("sessionEndTime")).getTime();

//                 if(!isNaN(startSeconds) && !isNaN(endSeconds)){
//                     var sessionTime = (endSeconds - startSeconds)/1000;
//                     if(sessionTime < minTime){
//                         minTime = sessionTime;
//                     }
//                     if(sessionTime > maxTime){
//                         maxTime = sessionTime;
//                     }
//                 }
//             }
//             numSessions = sessions.length;

//         }).then(function (){
//             if(minTime != Infinity){
//                 targetObject.set("minSessionTime", minTime);
//             }

//             if(maxTime != 0){
//                 targetObject.set("maxSessionTime", maxTime);
//             }

//             targetObject.set("numSessions", numSessions);
//             targetObject.save();
//         }).then(function (){
//             statEntries(objects);
//         });  
//     }
// };
