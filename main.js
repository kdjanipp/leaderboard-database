var Mandrill = require('./mandrillTemplateSend.js');
var MailHelper = require('./sendMailHelper.js');
var BackgroundTasks = require ('./backgroundTasks.js');

function getSexPronoun(sex) {
    var options = { himher : "their", heshe : "", hisher : "them"};

    console.log(options);

    if (sex === "Male") {
        options.hisher = "his";
        options.himher = "him";
        options.heshe = "he";

    } else if (sex === "Female") {
        options.hisher = "her";
        options.himher = "her";
        options.heshe = "she";
    }

    return options;
}

function setSessionFinalized(dogObjectId, dayCareObjectId) {
    Parse.Cloud.useMasterKey();
    var querySessionSchedule = new Parse.Query("SessionSchedule");
    querySessionSchedule.equalTo("daycareId", dayCareObjectId);
    querySessionSchedule.equalTo("dogId", dogObjectId);
    querySessionSchedule.equalTo("sessionStatus", "new");    
    querySessionSchedule.ascending("createdAt");
    return querySessionSchedule;
}

Parse.Cloud.define("removeDogDaycareAssociation", function(request, response) {
    Parse.Cloud.useMasterKey();
    if (!request.params.dogId) {
        response.error("Must have a dogId")
        return;
    }


    var query = new Parse.Query("Dog");
    query.get(request.params.dogId, {
        success: function(dog) {
            if (dog) {
                dog.set("currentDayCareId", null);
                dog.save({
                    success: function (dog) {
                        response.success("Association removed successfully");
                    },
                    error: function (myObject, error) {
                        response.error("Error Removing Association" + error);
                    }
                });
            } else {
                response.error("No Dog Found with ObjectId" + request.params.dogId);
            }
        },
        error: function(error) {
            response.error("Error Querying Dog" + error);
        }
    });
});

Parse.Cloud.define("signUpNewUserFromGingr", function(request, response) {
    var firstname = "";
    var lastname = "";
    var emailAddress = "";
    var gingrUserId = "";

    if (!request.params.emailAddress) {
        response.error("Must have an email address")
        return;
    }
    emailAddress = request.params.emailAddress;

    if (!request.params.gingrUserId) {
        response.error("Must have an gingrUserId")
        return;
    }
    gingrUserId = request.params.gingrUserId;

    if (typeof(request.params.firstname) != "undefined") {
        firstname = request.params.firstname;
    }

    if (typeof(request.params.lastname) != "undefined") {
        lastname = request.params.lastname;
    }

    Parse.Cloud.useMasterKey();
    var user = new Parse.User();
    user.set("username", emailAddress);
    user.set("password", "puppodfromgingr");
    user.set("email", emailAddress);
    user.set("fromGingr", true);

    // other fields can be set just like with Parse.Object
    user.set("gingrUserId", gingrUserId);

    user.signUp(null, {
      success: function(user) {
        sendAccountCreationTemplate(
            emailAddress,
            emailAddress,
            "test dog"
            );
        response.success("user created from gingr successfully");
        // Hooray! Let them use the app now.
      },
      error: function(user, error) {
        // Show the error message somewhere and let the user try again.
        console.error("Error: " + error.code + " " + error.message);
        response.error("Error: " + error.code + " " + error.message);
      }
    });
 });

Parse.Cloud.define("clearUnprocessedPlayEvents", function(request, response) {
    BackgroundTasks.initialize();
    BackgroundTasks.clearUnProcessedPlayEvents(response);
});


Parse.Cloud.define("updateUserEmail", function(request, response) {
    if (!request.user) {
        response.error("Must be signed in to call this Cloud Function.")
        return;
    }

    Parse.Cloud.useMasterKey();
    var query = new Parse.Query(Parse.User);
    query.equalTo("objectId", request.params.userId);
    query.find({
        success: function(user) {
            if(user && user.length==1){
                user[0].set("email", request.params.email);
                user[0].save(null, {
                    success: function(user) {
                        response.success("User successfully updated.");
                    },
                    error: function(user, error) {
                        response.error("Could not save changes to user.");
                    }
                });
            }
        },
        error: function(error) {
            response.error("Could not find user.");
        }
    });
 });

function sendTemplate(
    templateName,
    toName,
    toEmail,
    dogName,
    dogObjectId,
    dogAvatar,
    sex,
    response) {


    var Mandrill = require('./mandrillTemplateSend.js');
    Mandrill.initialize('owe6DTQFhKkrecs9IvNK4w'); //test key q1YgyXbudOw5YtyB4jSqhQ // realkey jxPud9IbFAgKvyOpwL_k1A
    console.log("dogname: " + dogName);
    var subject = "Your dog is rocking PupPod";
    var current_year = "2016"
    if (dogName) {
        subject = dogName + " is rocking PupPod";
    }

    var dogAvatarUrl = 'http://puppod.com/puppodWebApp/img/dogIcon.png';

    try {
        dogAvatarUrl = dogAvatar.url();
    } catch (e) {
        console.error(e);
    }

    var options = null;
    try {
        options = getSexPronoun(sex);
        console.log(options);
    } catch (e) {
        console.error(e);
    }

    Mandrill.sendTemplate({
        template_name: templateName,
        template_content: [{
            name: "example name",
            content: "example content" //Those are required but they are ignored
        }],
        message: {
              "global_merge_vars": [
            {
                "name": "subject",
                "content": subject
            },
            {
                "name": "current_year",
                "content": current_year
            },
            {
                "name": "dogname",
                "content": dogName
            },
            {
                "name": "dogobjectid",
                "content": dogObjectId
            },
            {
                "name": "avatar",
                "content": encodeURI(dogAvatarUrl)
            },
            {
                "name": "hisher",
                "content": options.hisher
            },
            {
                "name": "heshe",
                "content": options.heshe
            },
            {
                "name": "himher",
                "content": options.himher
            }
            ],
            to: [{
                email: toEmail,
                name: toName
            }],
            important: true
        },
        async: false
    }, {
        success: function (httpResponse) {
            console.log(httpResponse);
            response.success("Email sent!");
        },
        error: function (httpResponse) {
            console.error(httpResponse);
            response.error("Uh oh, something went wrong");
        }
    });
};

function sendAccountCreationTemplate(
    toName,
    toEmail,
    dogName,
    response) {
    var Mandrill = require('./mandrillTemplateSend.js');
    Mandrill.initialize('owe6DTQFhKkrecs9IvNK4w'); //test key q1YgyXbudOw5YtyB4jSqhQ // realkey jxPud9IbFAgKvyOpwL_k1A
    console.log("toName: " + toName);
    var subject = "Welcome to the PupPod family!";
    if (dogName) {
        subject = dogName + "has joined the PupPod pack";
    } else {
        dogName = "Your dog";
    }

    var current_year = "2016";
    var himher = "them";
    var hisher = "their";
    var password = "puppodfromgingr";
    var templateName = "accountcreationtemplate";

    Mandrill.sendTemplate({
        template_name: templateName,
        template_content: [{
            name: "example name",
            content: "example content" //Those are required but they are ignored
        }],
        message: {
              "global_merge_vars": [
            {
                "name": "subject",
                "content": subject
            },
            {
                "name": "current_year",
                "content": current_year
            },
            {
                "name": "dogsname",
                "content": dogName
            },
            {
                "name": "username",
                "content": toEmail
            },
            {
                "name": "password",
                "content": password
            },
            {
                "name": "himher",
                "content": himher
            },
            {
                "name": "hisher",
                "content": hisher
            }

            ],
            to: [{
                email: toEmail,
                name: toName
            }],
            important: true
        },
        async: false
    }, {
        success: function (httpResponse) {
            console.log(httpResponse);
            response.success("Email sent!");
        },
        error: function (httpResponse) {
            console.error(httpResponse);
            response.error("Uh oh, something went wrong");
        }
    });
};

Parse.Cloud.define("sendMail", function(request, response) {
    console.log(request);

    var dogObjectId = null;
    try {
        dogObjectId = request.params.dogObjectId;
    } catch (e) {
        console.error("no dogObj param in the request" + e);
        response.error("no dogObj param in request");
    }
    var query = new Parse.Query("Dog");
    query.get(dogObjectId, {useMasterKey:true}).then(
        function (dogObj) {
            if (dogObj) {
                var userId = dogObj.get("ownerObjectId");
                var dogName = dogObj.get("name");
                var dogAvatar = dogObj.get("avatar");
                var sex = dogObj.get("dogSex");

                var userQuery = new Parse.Query(Parse.User);
                console.log("user id " + userId);
                userQuery.get(userId, {useMasterKey:true}).then(
                    function (userObj) {
                        if (userObj) {
                            var email = userObj.get("email");
                            var emailOptedIn = userObj.get("dogNotification")
                            if (!email) {
                                email = userObj.get("username");
                                console.warn("email field in backend not populated. trying username")
                            }
                            console.log("email:" + email);
                            var name = null;
                            try {
                                var firstname = userObj.get("firstname");
                                var lastname = userObj.get("lastname");
                                name = firstname + " " + lastname;
                            } catch (e) {
                                name = email;
                            }
                            // only send message if email is opted in
                            if (emailOptedIn != false) {
                                sendTemplate(
                                    "PlaySessionNotification2",
                                    name,
                                    email,
                                    dogName,
                                    dogObjectId,
                                    dogAvatar,
                                    sex,
                                    response);
                            } else {
                                response.error("USER HAS NOT OPTED INTO EMAIL");
                            }

                        } else {
                            console.error("NULL USER FROM QUERY");
                            response.error("USER NOT FOUND IN THE SYSTEM!!");
                        }
                    },
                    function  (errorUserObj) {
                        console.error(errorUserObj);
                        response.error("USER NOT FOUND IN THE SYSTEM!!");
                    });


            } else {
                console.error("NULL DOG FROM QUERY");
                response.error("DOG NOT FOUND IN THE SYSTEM!");
            }
        },
        function  (errorDogQuery) {
            console.error(errorDogQuery);
            response.error("DOG NOT FOUND IN THE SYSTEM!!");
        });
});

function findTheLatestLot(dogObjectId, dayCareObjectId) {
    var queryTransaction = new Parse.Query("PupCreditTransaction");
    queryTransaction.equalTo("dayCareObjectId", dayCareObjectId);
    queryTransaction.equalTo("dogObjectId", dogObjectId);
    queryTransaction.greaterThan("lotBalance", 0);
    queryTransaction.ascending("createdAt");

    console.log("findTheLatestLot - Entering");

    return queryTransaction;
}



function decrementPupCredit(dogObjectId, dayCareObjectId, response) {
    var queryTransaction = new Parse.Query("PupCreditTransaction");
    queryTransaction.equalTo("dayCareObjectId", dayCareObjectId);
    queryTransaction.equalTo("dogObjectId", dogObjectId);
    queryTransaction.descending("createdAt");
    var transactionError = null;

    console.log("decrementPupCredit - Entering");

    queryTransaction.first({useMasterKey:true}).then(
        function (transaction) {
            console.log("inside transaction" + transaction);
            var pupBalance = 0;
            var revenueShare = 0;
            var pricePerPupCredit = 0;
            var entityPaidTo = null;

            try {
                pupBalance = transaction.get("pupCreditBalance");

                if (pupBalance > 0) {
                    --pupBalance;
                } else {
                    --pupBalance;
                    // TODO: For now allow pupBalance to go negative.
                    console.log("GAMEPLAY WITH ZERO PUPCREDITS DETECTED!!")
                }
            } catch (e) {
                transactionError = "NO MATCHING TRANSACTION FOR Decrement";
                console.error(transactionError + e);
            }

            findTheLatestLot(dogObjectId, dayCareObjectId).first({useMasterKey:true}).then(
                function (lotTransaction) {
                    console.log("inside lot transaction" + lotTransaction);
                    try {
                        if (!lotTransaction) {
                            revenueShare = transaction.get("revenueShare");
                            pricePerPupCredit = transaction.get("pricePerPupCredit");
                            entityPaidTo = transaction.get("entityPaidTo");
                        } else {
                            revenueShare = lotTransaction.get("revenueShare");
                            pricePerPupCredit = lotTransaction.get("pricePerPupCredit");
                            entityPaidTo = lotTransaction.get("entityPaidTo");
                        }

                        var PupCreditTransaction = Parse.Object.extend("PupCreditTransaction");
                        var newTransaction = new PupCreditTransaction();

                        var daycareShare = (1*pricePerPupCredit*revenueShare);

                        // Init to defautl values. The session as just started.
                        newTransaction.set("pupCreditEventType", "decrementPupCredit");
                        newTransaction.set("dayCareObjectId", dayCareObjectId);
                        newTransaction.set("dogObjectId", dogObjectId);
                        newTransaction.set("pupCreditsInTransaction", 1);
                        newTransaction.set("pupCreditBalance", pupBalance);
                        newTransaction.set("revenueShare", revenueShare);
                        newTransaction.set("pricePerPupCredit", pricePerPupCredit);
                        newTransaction.set("entityPaidTo", entityPaidTo);
                        newTransaction.set("daycareShare", daycareShare);

                        newTransaction.save().then (
                            function  (newTransactionObj) {
                                console.log("newTransactionObj = " + newTransactionObj);

                                if (lotTransaction) {
                                    // decrement the lot value and save it in the lotTransaction
                                    lotTransaction.increment("lotBalance", -1);
                                    lotTransaction.save(null, {useMasterKey:true}).then (
                                        function  (savedObj) {
                                            console.log("lot update saved successfully")
                                            if (savedObj) {
                                                if (response) {
                                                    response.success("success decrementPupCredit");
                                                }
                                            } else {
                                                console.error("lot update failed")
                                                if (response) {
                                                    response.error("decrement succeeded! lot updated failed!");
                                                }
                                            }
                                        },
                                        function  (errSessionSave) {
                                            console.error(errSessionSave);
                                            if (response) {
                                                response.error("decrement succeeded! lot updated failed!");
                                            }
                                        });
                                } else {
                                    if (response) {
                                        response.success("decrement succeeded! matching lot transaction not found! likely negative pupcredits");
                                    }
                                }
                            },

                            function (error) {
                                console.error(error);
                                if (response) {
                                    response.error("Critical: Failed to commit new pup transaction");
                                }
                            });
                    } catch (e) {
                        console.error("Critical: Failed to commit pup transaction");
                        console.error(e);
                        if (response) {
                            response.error("Critical: Failed to commit pup transaction");
                        }
                    }
                },
                function  (errLotTransaction) {
                    console.error(errLotTransaction);
                    if (response) {
                        response.error("Critical: Failed to find a lot transaction");
                    }
                });

            if (transactionError) {
                if (response) {
                    response.error(transactionError);
                }
            }
        },
        function  (errTransaction) {
            transactionError = "Critical Error : Failed to query existing transaction table";
            console.error(errTransaction);
            if (response) {
                response.error(transactionError);
            }
        });
}

Parse.Cloud.define("decrementPupCreditCloud", function(request, response) {
   // params
   // request.params.dayCareObjectId
   // request.params.dogObjectId
    Parse.Cloud.useMasterKey();

    console.log(request);

    var dogObjectId = null;
    try {
        dogObjectId = request.params.dogObjectId;
    } catch (e) {
        console.error("no dogObj param in the request" + e);
        response.error("no dogObj param in request");
    }

    var dayCareObjectId = null;
    try {
        dayCareObjectId = request.params.dayCareObjectId;
    } catch (e) {
        console.error("no eventName in the request" + e);
        response.error("no eventName in request");
    }

    if (dogObjectId && dayCareObjectId && response) {
        decrementPupCredit(dogObjectId, dayCareObjectId, response);
    } else {
        response.error("invalid request");
    }
});

Parse.Cloud.define("getRemainingPupCredits", function(request, response) {
   // params
   // request.params.dayCareObjectId
   // request.params.dogObjectId
    Parse.Cloud.useMasterKey();

    console.log(request);

    var dogObjectId = null;
    try {
        dogObjectId = request.params.dogObjectId;
    } catch (e) {
        console.error("no dogObj param in the request" + e);
    }

    var dayCareObjectId = null;
    try {
        dayCareObjectId = request.params.dayCareObjectId;
    } catch (e) {
        console.error("no eventName in the request" + e);
    }

    var queryTransaction = new Parse.Query("PupCreditTransaction");
    queryTransaction.equalTo("dayCareObjectId", request.params.dayCareObjectId);
    queryTransaction.equalTo("dogObjectId", request.params.dogObjectId);
    queryTransaction.descending("createdAt");
    queryTransaction.first({useMasterKey:true}).then(
        function (transaction) {
            console.log("transaction inside" + transaction);

            var pupBalance = 0.0;
            if (transaction != null) {
                try {
                    pupBalance = transaction.get("pupCreditBalance");
                } catch (e) {
                    console.error(e);
                    console.log("no transaction for dog and daycare");
                }
            }
            response.success(pupBalance);
        },
        function  (errTransaction) {
            response.error(transactionError);
            console.error(errTransaction);
        });
});

Parse.Cloud.define("addPurchasedPupCredits", function(request, response) {
  // params
  // request.params.dayCareObjectId
  // request.params.dogObjectId
  // request.params.pupPackageObjectId

  // query the pupCreditPackage
  //  - just call get by id
  //  from here you add the numPupCredits, pupCreditsPrice, revenueShare

  // query the last transaction
          //params
              //dogObjectId
              //dayCareObjectId
          // sort latest
          // first
    Parse.Cloud.useMasterKey();

    console.log(request);

    var paramErrors = null;

    var dogObjectId = null;
    try {
        dogObjectId = request.params.dogObjectId;

    } catch (e) {
        paramErrors = "no dogObj param in the request";
        console.error(paramErrors + e);
        response.error(paramErrors);
        return;
    }

    var dayCareObjectId = null;
    try {
        dayCareObjectId = request.params.dayCareObjectId;
    } catch (e) {
        paramErrors = "no dayCareObjectId param in the request";
        console.error(paramErrors + e);
        response.error(paramErrors);
        return;
    }

    var pupPackageObjectId = null;
    try {
        pupPackageObjectId = request.params.pupPackageObjectId;
    } catch (e) {
        paramErrors = "no packageObjectId in the request";
        console.error(paramErrors + e);
        response.error(paramErrors);
        return;
    }

    var pupCreditsPrice = null;
    var revenueShare = null;
    var numPupCredits = null;
    var transactionError = null;

    console.log("pupPackageObjectId = " + pupPackageObjectId);
    var query = new Parse.Query("PupCreditPackage");
    query.get(pupPackageObjectId, {useMasterKey:true}).then(
        function (packageObj) {
            console.log("packageObj inside:" + packageObj);
            pupCreditsPrice = packageObj.get("pricePerPupCredit");
            numPupCredits = packageObj.get("numPupCreditsPerPackage");
            revenueShare = packageObj.get("revenueShare");
            transactionError = null;

            var queryTransaction = new Parse.Query("PupCreditTransaction");
            queryTransaction.equalTo("dayCareObjectId", dayCareObjectId);
            queryTransaction.equalTo("dogObjectId", dogObjectId);
            queryTransaction.descending("createdAt");
            queryTransaction.first({useMasterKey:true}).then(
                function (transaction) {
                    console.log("transaction inside" + transaction);

                    var pupBalance = 0;
                    var daycareShare = 0;
                    var lotBalance = numPupCredits;

                    try {
                        pupBalance = transaction.get("pupCreditBalance");
                        var tempPupBalance = pupBalance;
                        pupBalance += numPupCredits;

                        if (tempPupBalance < 0) {
                            var numPupCreditsToCharge = 0;
                            if (pupBalance < 0) {
                                // this means that the pupBalance is still negative.
                                // the only amount we can charge in this case is
                                // the amount that was added.
                                numPupCreditsToCharge = numPupCredits;
                                lotBalance = 0;
                            } else {
                                // our pupbalance is positive.
                                // this means the user bought more pupCredits than
                                // the  -ve pup credit balance that existed in the account.
                                // thus we only charge the -ve balance in the daycareshare.
                                // we also need to update the lotBalance to the renaming value.
                                numPupCreditsToCharge = (-1)*tempPupBalance;
                                lotBalance = numPupCredits + tempPupBalance;
                            }

                            daycareShare = (numPupCreditsToCharge*pupCreditsPrice*revenueShare);
                        }

                    } catch (e) {
                        pupBalance = numPupCredits;
                        console.log("first transaction for dog and daycare");
                    }

                    try {
                        var PupCreditTransaction = Parse.Object.extend("PupCreditTransaction");
                        var newTransaction = new PupCreditTransaction();

                        // Init to default values. The session as just started.
                        newTransaction.set("pupCreditEventType", "addPupCredits");
                        newTransaction.set("dayCareObjectId", dayCareObjectId);
                        newTransaction.set("dogObjectId", dogObjectId);
                        newTransaction.set("pupCreditsInTransaction", numPupCredits);
                        newTransaction.set("pupCreditBalance", pupBalance);
                        newTransaction.set("revenueShare", revenueShare);
                        newTransaction.set("pricePerPupCredit", pupCreditsPrice);
                        newTransaction.set("entityPaidTo", "PupPod");
                        newTransaction.set("lotBalance", lotBalance);
                        newTransaction.set("daycareShare", daycareShare);

                        newTransaction.save().then (
                            function  (newTransactionObj) {
                                console.log("newTransactionObj = " + newTransactionObj);
                                response.success("success");
                            },

                            function (error) {
                                console.error(error);
                                response.error("Critical: Failed to commit transaction");
                            });
                    } catch (e) {
                        transactionError = "Critical Error : Failed to construct transaction";
                        console.error(e);
                    }

                    if (transactionError) {
                        response.error(transactionError);
                    }
                },
                function  (errTransaction) {
                    transactionError = "Critical Error : Failed to query existing transaction table";
                    console.error(errTransaction);
                    response.error(transactionError);
                });
        },
        function  (packageObjErr) {
            transactionError = "Error getting package. Has this package been created in parse?"
            console.error(packageObjErr);
            response.error(transactionError);
            return;
        });

});

function updateLifeTimeStats(dogObj, currentSessionObj, dispensesCount) {
    // get the lifetime dispense count and calories on the dog table.
    var createdAtDate = null;
    var sessionEndTime = null;
    try {
        var createdAtObj = currentSessionObj.get("createdAt");
        var sessionEndTimeObj = currentSessionObj.get("updatedAt");

        createdAtDate = new Date(createdAtObj);
        sessionEndTime = new Date(sessionEndTimeObj);

    } catch (e) {
        createdAtDate = new Date();
        sessionEndTime = new Date();
        console.error(e);
    }

    // lifetimeElapsedTime
    var elapsedPlayTime = sessionEndTime - createdAtDate;
    console.log("elapsedTime" + elapsedPlayTime);
    dogObj.increment("lifetimePlayTimeInMillis", elapsedPlayTime);

    dogObj.increment("lifetimeDispenseCount", dispensesCount);

    var currentCalories = (dispensesCount*10);

    dogObj.increment("lifetimeCalories", currentCalories);

    return dogObj;
}

Parse.Cloud.afterSave("PupCreditTransaction", function(request) {

    // the request object is for an incoming.
    /*
    Sample Json
    {
    "dogObjectId": "9PQs2FY0JH",
    "dayCareObjectId": "dayCareObjectId",
    "pupCreditBalance": "pupCreditBalance",
    }
    */
    console.log(request);

    var dogObjectId = null;
    try {
        dogObjectId = request.object.get("dogObjectId");

    } catch (e) {
        console.error("no dogObj in the request" + e);
    }

    var dayCareObjectId = null;
    try {
        dayCareObjectId = request.object.get("dayCareObjectId");
    } catch (e) {
        console.error("no dayCareObjectId in the request" + e);
    }

    var pupCreditBalance = null;
    try {
        pupCreditBalance = request.object.get("pupCreditBalance");
    } catch (e) {
        console.error("no pupCreditBalance in the request" + e);
    }

    var createdAt = null;
    try {
        createdAt = request.object.get("createdAt");

    } catch (e) {
        console.error("no createdAt in the request" + e);
    }

    var updatedAt = null;
    try {
        updatedAt = request.object.get("updatedAt");

    } catch (e) {
        console.error("no updatedAt in the request" + e);
    }

    // Create the game session as this is a new session event.
    var query = new Parse.Query("DogPupCreditView");
    query.equalTo("dayCareObjectId", dayCareObjectId).
      equalTo("dogObjectId", dogObjectId);

    query.first({useMasterKey:true}).then(
    function (dogPupCreditView) {

        console.log("dogPupCreditView inside" + dogPupCreditView);

        if (dogPupCreditView) {
            dogPupCreditView.set("pupCreditBalance", pupCreditBalance);
        } else {
            var DogPupCreditView = Parse.Object.extend("DogPupCreditView");
            dogPupCreditView = new DogPupCreditView();
            dogPupCreditView.set("pupCreditBalance", pupCreditBalance);
            dogPupCreditView.set("dogObjectId", dogObjectId);
            dogPupCreditView.set("dayCareObjectId", dayCareObjectId);
        }

        dogPupCreditView.save(null, {useMasterKey:true}).then (
            function  (savedObj) {
                if (savedObj) {
                    console.log("dogPupCreditView saved successfully");
                    var createdAtDate = new Date(createdAt);
                    var updatedAtDate = new Date(updatedAt);

                    if (createdAtDate.getTime() === updatedAtDate.getTime()) {
                        console.log("querying SessionSchedule to mark the booking as complete");

                        // now mark the session played.
                         ized(dogObjectId, dayCareObjectId).first({useMasterKey:true}).then(
                        function (sessionSchedule) {
                            console.log("session schedule finalized query" + sessionSchedule);
                            if (sessionSchedule) {
                                sessionSchedule.set("sessionStatus", "played");
                                sessionSchedule.save({
                                    success: function (sessionScheduleObj) {
                                        console.log("successfully saved obj");
                                    },
                                    error: function (sessionScheduleObj, error) {
                                        console.log("failed to save obj " + error);
                                    }
                                });
                            }
                        },
                        function (errorSessionSchedule) {
                            console.error(errorSessionSchedule);
                        });
                    }
                } else {
                    console.error("dogPupCreditView object could not save");
                }
            },
            function  (errSessionSave) {
                console.error(errSessionSave);
            });
    },

    function (errorDogView) {
        console.error(errorDogView);
    });
});

Parse.Cloud.afterSave("GamePlayEvent", function(request) {

    // the request object is for an incoming.
    /*
    Sample Json
    {
    "dogObjectId": "9PQs2FY0JH",
    "eventName": "HubStartedEvent",
    "deviceId": "testDeviceId",
    "dogName": "Brownie",
    "gameLevel": "5",
    "dispensesCount": 5,
    "beepsCount": 5,
    "sessionId": "testSessionId"
    }
    */
    console.log(request);

    var dogObjectId = null;
    try {
        dogObjectId = request.object.get("dogObjectId");

    } catch (e) {
        console.error("no dogObj in the request" + e);
    }

    var eventName = null;
    try {
        eventName = request.object.get("eventName");
    } catch (e) {
        console.error("no eventName in the request" + e);
    }

    var currentLevel = null;
    try {
        currentLevel = request.object.get("gameLevel");
    } catch (e) {
        console.error("no currentLevel in the request" + e);
    }

    var dispensesCount = null;
    try {
        dispensesCount = request.object.get("dispensesCount");
    } catch (e) {
        console.error("no dispensesCount in the request" + e);
    }

    var beepsCount = null;
    try {
        beepsCount = request.object.get("beepsCount");
    } catch (e) {
        console.error("no beepsCount in the request" + e);
    }

    var missesCount = null;
    try {
        missesCount = request.object.get("missesCount");
    } catch (e) {
        console.error("no missesCount in the request" + e);
    }

    var sessionId = null;
    try {
        sessionId = request.object.get("sessionId");
    } catch (e) {
        console.error("no sessionId in the request" + e);
    }

    var deviceId = null;
    try {
        deviceId = request.object.get("deviceId");
    } catch (e) {
        console.error("no deviceId in the request" + e);
    }

    if (eventName.toUpperCase() === "HUBSTARTEDEVENT") {
        // Create the game session as this is a new session event.
        var GamePlaySession = Parse.Object.extend("GamePlaySession");
        var newSession = new GamePlaySession();

        // Init to defaul values. The session as just started.
        newSession.set("calories", 0);
        newSession.set("sessionEndTime", new Date());
        newSession.set("successRate", 0);

        // Items from request from Hub
        newSession.set("dogObjectId", dogObjectId);
        newSession.set("dispenseCount", 0);
        newSession.set("beepsCount", 0);
        newSession.set("missesCount", 0);
        newSession.set("currentLevel", parseInt(currentLevel));
        newSession.set("deviceId", deviceId);
        newSession.set("sessionId", sessionId);
        newSession.set("sessionFinalized", false);

        newSession.save().then (
            // after the session object has been saved, get the currentSessionObj
            // and update the dog table
            function  (sessionSavedObj) {

                console.log("currentSession = " + sessionSavedObj);
                var currentSessionObj = sessionSavedObj;
                var query = new Parse.Query("Dog");
                query.get(dogObjectId, {useMasterKey:true}).then(
                    function (dogObj) {

                        console.log("currentSession inside:" + currentSessionObj);

                        // set the current game session on the dog table.
                        dogObj.set("currentGameSession", currentSessionObj.id);
                        dogObj.save(null, {useMasterKey:true}).then(
                            function  (savedObj) {
                                console.log("savedDogObj = " + savedObj);
                            },
                            function  (errorDogSave) {
                                console.error(errorDogSave);
                            });
                    },
                    function  (errorDogQuery) {
                        console.error(errorDogQuery);
                    });
            },

            function (error) {
                console.error(error);
            });

    } else if (eventName.toUpperCase() === "HUBSTOPPEDEVENT") {
        // Create the game session as this is a new session event.
        var query = new Parse.Query("GamePlaySession");
        query.equalTo("sessionId", sessionId).
          equalTo("dogObjectId", dogObjectId);

        query.first({useMasterKey:true}).then(
        function (currentSessionObj) {

            console.log("currentSession inside" + currentSessionObj);

            currentSessionObj.set("dispenseCount", dispensesCount);
            currentSessionObj.set("calories", dispensesCount*10);
            currentSessionObj.set("sessionEndTime", new Date());
            currentSessionObj.set("currentLevel", parseInt(currentLevel));
            currentSessionObj.set("beepsCount", beepsCount);
            currentSessionObj.set("missesCount", missesCount);
            currentSessionObj.set("sessionFinalized", true);

            var successRate = 0;
            if (beepsCount > 0) {
                successRate = ((beepsCount - missesCount)/beepsCount) * 100;
            } else {
                console.error("HUBSTOPPEDEVENT beepsCount is Zero");
            }
            currentSessionObj.set("successRate",  successRate);

            currentSessionObj.save(null, {useMasterKey:true}).then (
                // after the session object has been saved, get the currentSessionObj
                // and update the dog table
                function  (sessionSavedObj) {
                    var query = new Parse.Query("Dog");

                    query.get(dogObjectId, {useMasterKey:true}).then(
                        function (dogObj) {
                            // set the current game session on the dog table.
                            dogObj.set("currentGameSession", currentSessionObj.id);

                            // update lifetimestats
                            dogObj = updateLifeTimeStats(dogObj, currentSessionObj, dispensesCount);

                            // decrement day care credits.
                            var dayCareObjectId = null;
                            dayCareObjectId = dogObj.get("currentDayCareId");
                            decrementPupCredit(dogObjectId, dayCareObjectId, null);

                            // save dog object
                            dogObj.save(null, {useMasterKey:true}).then(
                                function  (savedObj) {
                                    console.log("savedDogObj = " + savedObj);
                                },
                                function  (errorDogSave) {
                                    console.error(errorDogSave);
                                });
                        },
                        function  (errorQueryDog) {
                            console.error(errorQueryDog);
                        });
                },
                function  (errSessionSave) {
                    console.error(errSessionSave);
                });
        },

        function (errGamePlaySessionQuery) {
            console.error(errGamePlaySessionQuery);
        });

    }
});


Parse.Cloud.afterSave(Parse.User, function(request) {
    Parse.Cloud.useMasterKey();

    // the request object is for an incoming.
    /*
    Sample Json
    {
    "dogObjectId": "9PQs2FY0JH",
    "eventName": "HubStartedEvent",
    "deviceId": "testDeviceId",
    "dogName": "Brownie",
    "gameLevel": "5",
    "dispensesCount": 5,
    "beepsCount": 5,
    "sessionId": "testSessionId"
    }
    */
    console.log(request);

    var creationMailSent = null;
    try {
        creationMailSent = request.object.get("creationMailSent");

    } catch (e) {
        console.error("no creationMailSent param in the request" + e);
    }

    if (!creationMailSent) {
        var Mandrill = require('./mandrillTemplateSend.js');
        Mandrill.initialize('owe6DTQFhKkrecs9IvNK4w'); //test key q1YgyXbudOw5YtyB4jSqhQ // realkey jxPud9IbFAgKvyOpwL_k1A
        MailHelper.initialize(Mandrill);
        MailHelper.sendMailOnAccountCreation(request);
    }
});

//Leaderboard

/*
    Creating dogObjectId column to make certain queries easier
*/

Parse.Cloud.define("dogID", function(request, response){
    Parse.Cloud.useMasterKey();
    var leaderboardQuery = new Parse.Query("Leaderboard");
    leaderboardQuery.include("Dog");
    leaderboardQuery.each(function (dog){
        var dogId = dog.get("Dog").id;
        dog.set("dogObjectId", dogId);
        return dog.save();
    }).then(function () {
		// Set the job's success status
		response.success("Success.");
	}, function (error) {
		// Set the job's error status
		response.error("Error.");
	});
});

/*
    Creates an entry in the leaderboard table after a gameplaysession is completed
*/

Parse.Cloud.afterSave("GamePlaySession", function(request){
    Parse.Cloud.useMasterKey();
    var session = request.object;

    var dogId = session.get("dogObjectId");
    var level = session.get("currentLevel");
    if(dogId == undefined || level == undefined){
        return;
    }

    var missedBeeps = session.get("missesCount");
    if(isNaN(missedBeeps)){
        missedBeeps = 0;
    }

    var totalBeeps = session.get("beepsCount");
    if(isNaN(totalBeeps)){
        totalBeeps = 0;
    }

    var dispenseCount = session.get("dispenseCount");
    if(isNaN(dispenseCount)){
        dispenseCount = 0;
    }
    
    var startTime = new Date(session.get("createdAt").getTime());
    var endTime = new Date(session.get("sessionEndTime").getTime());
    var milliseconds = 0;
    if(!isNaN(startTime) && !isNaN(endTime)){
        milliseconds = endTime - startTime;
    }

    var dogQuery = new Parse.Query("Dog")
    dogQuery.equalTo("objectId", dogId);

    var leaderboardQuery = new Parse.Query("Leaderboard");
    leaderboardQuery.matchesQuery("Dog", dogQuery);
    
    //Updating all the columns of the matching row
    leaderboardQuery.first().then(function (row){
        row.increment("lifetimePlayTimeInMillis", milliseconds);
        row.increment("lifetimeDispenseCount", dispenseCount);
        if(level == 1){ //If game play session was level 1
            row.increment("level1Seconds", milliseconds/1000);
            row.increment("level1Dispenses", dispenseCount);
            row.set("level1Score", row.get("level1Seconds")/row.get("level1Dispenses"));
            
            //Updating achievements
            if(dispenseCount > 5 && row.get("level1Achievement5Dispenses") != true){
                row.set("level1Achievement5Dispenses", true);
            }
            if(dispenseCount > 10 && row.get("level1Achievement10Dispenses") != true){
                row.set("level1Achievement10Dispenses", true);
            }
            if(dispenseCount > 20 && row.get("level1Achievement20Dispenses") != true){
                row.set("level1Achievement10Dispenses", true);
            }
        }else if(level == 2){ //If game play session was level 2
            row.increment("level2Seconds", milliseconds/1000);
            row.increment("level2Dispenses", dispenseCount);
            row.set("level2Score", row.get("level2Seconds")/row.get("level2Dispenses"));

            if(dispenseCount > 5 && row.get("level2Achievement5Dispenses") != true){
                row.set("level2Achievement5Dispenses", true);
            }
            if(dispenseCount > 10 && row.get("level2Achievement10Dispenses") != true){
                row.set("level2Achievement10Dispenses", true);
            }
            if(dispenseCount > 20 && row.get("level2Achievement20Dispenses") != true){
                row.set("level2Achievement10Dispenses", true);
            }
        }else if(level == 3){ //If game play session was level 3
            row.increment("level3Seconds", milliseconds/1000);
            row.increment("level3Dispenses", dispenseCount);
            row.set("level3Score", (row.get("level3Beeps") * row.get("level3Score") + (totalBeeps - missedBeeps)) / 
                (row.get("level3Score") + totalBeeps));
            
            if(missedBeeps == 0 && totalBeeps > 5 && row.get("level3AchievementPerfect5") != true){
                row.set("level3AchievementPerfect5", true);
            }
            if(missedBeeps == 0 && totalBeeps > 10 && row.get("level3AchievementPerfect10") != true){
                row.set("level3AchievementPerfect10", true);
            }
            if(missedBeeps == 0 && totalBeeps > 20 && row.get("level3AchievementPerfect20") != true){
                row.set("level3AchievementPerfect20", true);
            }
        }else if(level == 4){ //If game play session was level 4
            row.increment("level4Seconds", milliseconds/1000);
            row.increment("level4Dispenses", dispenseCount);
            row.set("level4Score", (row.get("level4Beeps") * row.get("level4Score") + (totalBeeps - missedBeeps)) / 
                (row.get("level4Score") + totalBeeps));

            if(missedBeeps == 0 && totalBeeps > 5 && row.get("level4AchievementPerfect5") != true){
                row.set("level4AchievementPerfect5", true);
            }
            if(missedBeeps == 0 && totalBeeps > 10 && row.get("level4AchievementPerfect10") != true){
                row.set("level4AchievementPerfect10", true);
            }
            if(missedBeeps == 0 && totalBeeps > 20 && row.get("level4AchievementPerfect20") != true){
                row.set("level4AchievementPerfect20", true);
            }
        }else if(level == 5){ //If game play session was level 5
            row.increment("level5Seconds", milliseconds/1000);
            row.increment("level5Dispenses", dispenseCount);
            row.set("level5Score", (row.get("level5Beeps") * row.get("level5Score") + (totalBeeps - missedBeeps)) / 
                (row.get("level5Score") + totalBeeps));

            if(missedBeeps == 0 && totalBeeps > 5 && row.get("level5AchievementPerfect5") != true){
                row.set("level5AchievementPerfect5", true);
            }
            if(missedBeeps == 0 && totalBeeps > 10 && row.get("level5AchievementPerfect10") != true){
                row.set("level5AchievementPerfect10", true);
            }
            if(missedBeeps == 0 && totalBeeps > 20 && row.get("level5AchievementPerfect20") != true){
                row.set("level5AchievementPerfect20", true);
            }
        }
        row.save();
    });

});
/*
    Updating the leaderboard table after a shaping event is completed
*/

Parse.Cloud.afterSave("ShapingEvent", function(request){
    Parse.Cloud.useMasterKey();
    var event = request.object;

    var type = event.get("eventType");
    var beepedOnce = event.get("hasBeepedAtleastOnce");
    var level = event.get("gameLevel");

    if(type != "Motion" || beepedOnce != true || level < 3){
        return;
    }

    var time = event.get("millisSinceLastBeep");
    var positiveLastBeep = event.get("wasLastBeepPositive");
    var dogId = event.get("dogObjectId");

    var leaderboardQuery = new Parse.Query("Leaderboard");
    leaderboardQuery.equalTo("dogObjectId", dogId);

    leaderboardQuery.first().then(function (row){
        if(level == 3){
            if(time < 3000){
                row.increment("level3ShapingSuccess");
            }else{
                row.increment("level3ShapingFailure");
            }
            row.set("level3ShapingScore", (row.get("level3ShapingSuccess") / (row.get("level3ShapingSuccess") + row.get("level3ShapingFailure"))));
        }else if(level == 4){
            if(positiveLastBeep == true){
                row.increment("level4ShapingSuccess");
            }else{
                row.increment("level4ShapingFailure");
            }
            row.set("level4ShapingScore", (row.get("level4ShapingSuccess") / (row.get("level4ShapingSuccess") + row.get("level4ShapingFailure"))));
        }else if(level == 5){
            if(positiveLastBeep == true){
                row.increment("level5ShapingSuccess");
            }else{
                row.increment("level5ShapingFailure");
            }
            row.set("level5ShapingScore", (row.get("level5ShapingSuccess") / (row.get("level5ShapingSuccess") + row.get("level5ShapingFailure"))));
        }
        row.save();
    });
});

/*
    Building the shaping score columns in the leaderboard class
    *** Only used in the initial construction of the table
*/

// Parse.Cloud.define("shapingScore", function(request, response){
//     Parse.Cloud.useMasterKey();
//     var leaderboardQuery = new Parse.Query("Leaderboard");
//     var shapingQuery = new Parse.Query("ShapingEvent");

//     leaderboardQuery.each(function (dog){

//         console.log(dog);

//         shapingQuery.equalTo("eventType", "Motion");
//         shapingQuery.equalTo("hasBeepedAtLeastOnce", true);
//         shapingQuery.containedIn("gameLevel", ["3","4","5"]);
//         shapingQuery.equalTo("dogObjectId", dog.get("dogObjectId"));
//         shapingQuery.limit(1000);

//         var level3Success = 0;
//         var level3Failure = 0;
//         var level4Success = 0;
//         var level4Failure = 0;
//         var level5Success = 0;
//         var level5Failure = 0;

//         shapingQuery.find().then(function (events){
//             for(var i = 0; i < events.length; i++){
//                 var level = parseInt(events[i].get("gameLevel"));
//                 var time = events[i].get("millisSinceLastBeep");
//                 var lastBeep = events[i].get("wasLastBeepPositive");

//                 if(level == 3){
//                     if(time < 3000){
//                         level3Success++;
//                     }else{
//                         level3Failure++;
//                     }
//                 }else if(level == 4){
//                     if(lastBeep == true){
//                         level4Success++;
//                     }else{
//                         level4Failure++;
//                     }
//                 }else if(level == 5){
//                     if(lastBeep == true){
//                         level5Success++;
//                     }else{
//                         level5Failure++;
//                     }
//                 }
//             }
//         }).then(function (){
//             dog.set("level3ShapingSuccess", level3Success);
//             dog.set("level3ShapingFailure", level3Failure);
//             dog.set("level4ShapingSuccess", level4Success);
//             dog.set("level4ShapingFailure", level4Failure);
//             dog.set("level5ShapingSuccess", level5Success);
//             dog.set("level5ShapingFailure", level5Failure);

//             dog.set("level3ShapingScore", level3Success / (level3Success + level3Failure));
//             dog.set("level4ShapingScore", level4Success / (level4Success + level4Failure));
//             dog.set("level5ShapingScore", level5Success / (level5Success + level5Failure));

//             return dog.save();
//             //        Parse.Promise.resolve();
//         });
//     }).then(function () {
// 		// Set the job's success status
// 		response.success("Success.");
// 	}, function (error) {
// 		// Set the job's error status
// 		response.error("Error.");
// 	});
// });

